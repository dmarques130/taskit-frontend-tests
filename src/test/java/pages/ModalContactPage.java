package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ModalContactPage extends BasePage{
    public ModalContactPage(WebDriver driver) {
        super(driver);
    }
    public ModalContactPage selectType(String tipo) {
        WebElement modalAddMoreData = driver.findElement(By.id("addmoredata"));
        WebElement comboType = modalAddMoreData.findElement(By.name("type"));
        new Select(comboType).selectByVisibleText(tipo);
        return this;
    }

    public ModalContactPage takeContact(String contato) {
        driver.findElement(By.name("contact")).sendKeys(contato);
        return this;
    }

    public MePage clickSave() {
        driver.findElement(By.linkText("SAVE")).click();
        return new MePage(driver);
    }

    public MePage addContatc(String tipo, String contato) {
        this.selectType(tipo);
        this.takeContact(contato);
        return this.clickSave();
    }
}
