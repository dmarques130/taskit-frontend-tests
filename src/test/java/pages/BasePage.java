package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BasePage {
    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }
    //Your contact has been added!
    public String getMessageToast() {
        return driver.findElement(By.id("toast-container")).getText();
    }
}
