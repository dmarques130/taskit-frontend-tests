package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FormLoginPage extends BasePage {


    public FormLoginPage(WebDriver driver){
        super(driver);
    }

    public FormLoginPage typeLogin(String login) {
        driver.findElement(By.id("signinbox")).findElement(By.name("login")).sendKeys(login);
        return this;
    }

    public FormLoginPage typePassword(String password) {
        driver.findElement(By.id("signinbox")).findElement(By.name("password")).sendKeys(password);
        return this;
    }

    public SecretPage clickSignIn() {
        driver.findElement(By.id("signinbox")).findElement(By.linkText("SIGN IN")).click();
        return new SecretPage(driver);
    }

    public SecretPage fazerLogin(String login, String password) {
        this.typeLogin(login);
        this.typePassword(password);
        return clickSignIn();
    }
}
