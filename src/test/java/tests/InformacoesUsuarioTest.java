package tests;

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.LoginPage;
import support.Generator;
import support.Screenshot;

import static org.junit.Assert.assertEquals;

@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "informacoesUsuarioTest.csv")
public class InformacoesUsuarioTest extends BaseTest {

    @Test
    public void testAdicionarContatoUsuario(@Param(name="tipo")String tipo,
                                            @Param(name="contato")String contato,
                                            @Param(name="mensagem")String msg) {

        String getMessage = new LoginPage(driver)
                .clickSign()
                .fazerLogin("julio0001", "123456")
                .clickMe()
                .clickAbaMoreDataAboutYou()
                .clickButtonAddMoreData()
                .addContatc(tipo, contato)
                .getMessageToast();

        assertEquals(msg, getMessage);
    }

    //@Test
    public void testRemoverContatoUsuario(){
        driver.findElement(By.xpath("//span[text()=\"+5511953647212\"]/following-sibling::a")).click();
        driver.switchTo().alert().accept();

        WebElement alertaModal = driver.findElement(By.id("toast-container"));
        String mensagem = alertaModal.getText();
        assertEquals("Rest in peace, dear phone!", mensagem);

        Screenshot.take(driver, "C:\\reports\\taskit\\" + Generator.saveDateHourFile() +  "testRemoverContatoUsuario.png");

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.stalenessOf(alertaModal));

        driver.findElement(By.linkText("Logout")).click();
    }
}
