package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import support.Browser;

import java.net.MalformedURLException;

public class BaseTest {
    protected WebDriver driver;

    @Before
    public void setUp() throws MalformedURLException {
        //driver = Browser.createChrome();
        driver = Browser.createBrowserStack();
        driver.get("http://www.juliodelima.com.br/taskit/");
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
